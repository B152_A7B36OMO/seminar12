package seminar13.excercise3;

class Plus implements Expression {
    Expression left;
    Expression right;

    Plus(Expression l, Expression r) {
        left = l;
        right = r;
    }

    public int getValue() {
        int value = left.getValue() + right.getValue();
        System.out.println("vyhodnocuji plus: "+ value);
        return value;
    }
}
