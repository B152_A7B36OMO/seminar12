package seminar13.excercise3;

class Times implements Expression {
    Expression left;
    Expression right;

    Times(Expression l, Expression r) {
        left = l;
        right = r;
    }

    public int getValue() {
        int value = left.getValue() * right.getValue();
        System.out.println("vyhodnocuji nasobeni:" + value);
        return value;
    }
}
