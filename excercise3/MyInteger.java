package seminar13.excercise3;


class MyInteger implements Expression {
    int value;

    public MyInteger(int i) {
        value = i;
    }

    public int getValue() {
        System.out.println("integer " + value);
        return value;
    }
}
